# Halcon_LearningRecord

#### 介绍
记载一些在编程及图像处理过程中的问题及解决方案，主要包括QT与C++的一些实用小项目，Halcon及Opencv的图像处理相关解决方案，主要是自己学习的一些记录内容，只是用于学习，涉及的相关借鉴内容的我尽量标明出处，如涉及版权问题，请联系删除。

#### 软件环境
Qt：5.9.6
vs2019
Halcon 12.0/17.0


#### 子项目介绍

* 1.ARM_FontSet
**功能：**实现Halcon界面设定的文字字体和大小设置。

![ARM_FontSet](https://images.gitee.com/uploads/images/2021/0817/221328_6315a907_4968621.png "1.ARM_FontSet.PNG")

* 2.CustomView
**功能：**实现超大像素图像读取/显示功能。

![CustomView](https://images.gitee.com/uploads/images/2021/0817/221415_1799756c_4968621.png "2.CustomView.PNG")

* 3.ImageScale
**功能：**实现图像缩放平移功能。

![ImageScale](https://images.gitee.com/uploads/images/2021/0817/221456_5b998351_4968621.png "3.ImageScale.PNG")

* 4.Draw_Item
**功能：**实现QGraphicsView的自定义Item组件。

![Draw_Item](https://images.gitee.com/uploads/images/2021/0817/221520_bf81174f_4968621.png "4.Draw_Item.PNG")

* 5.AutoFacus
**功能：**实现图像自动对焦评价系数参考。

![AutoFacus](snap/5.AutoFacus.PNG)

* 6.LockerTest
**功能：**实现Qt抽屉展开折叠显示功能。

![LockerTest](snap/6.LockerTest.PNG)

* 7.CustomToolBox
**功能：**实现Qt控件操作使用功能。

![CustomToolBox](snap/7.CustomToolBox.PNG)

* 8.AutoRepeat_Test
**功能：**Qt实现QPushButton长按触发事件。

![AutoRepeat](snap/8.AutoRepeat.gif)

* 9.SwitchButton
**功能：**Qt实现开关控件效果。

![控件效果](snap/9.SwitchButton.PNG)

* 10.Balser_GigeDemo
**功能：**Balser工业相机采图及参数设置。

![Balser相机采图](snap/10.Balser_GigeDemo.PNG)

* 11.QtCameraDemo
**功能：**Qt实现USB多摄像头视频、抓图及参数设置。

![多相机采图和参数设置](snap/11.QtCameraDemo.gif)

* 12.VideoTracking
**功能：**Qt+OpenCV实现USB摄像头监测移动物体。

![VideoTracking](snap/12.VideoTracking.PNG)

* 13.GrayTo3channel
**功能：**Qt实现8位灰度图叠加通道转换成24位三通道图像。

![GrayTo3channel](snap/13.GrayTo3channel.gif)

#### 文件夹说明

1.  snap
此文件夹为各子工程界面截图
2.  File_Info
此文件夹存放工程所需文件，如图像/txt/ini
3.  bin
此文件夹存放工程创建的exe文件


